<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MicroPostRepository")
 * @ORM\HasLifecycleCallbacks();
 */
class MicroPost
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=200)
     */
    private $text;
    
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User" , inversedBy="posts")
     */
    private $user;

    /**
     * @ORM\Column(type="datetime")
     */
    private $time;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", inversedBy="postsLiked")
     * @ORM\JoinTable(name="post_likes",
     *    joinColumns={@ORM\JoinColumn(name="post_id", referencedColumnName="id")},
     *    inverseJoinColumns={@ORM\JoinColumn(name="user_id",referencedColumnName="id")}
     * )
     */
    private $likedBy;

    public function __construct()
    {
        $this->likedBy = new ArrayCollection();
    }
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */ 
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param mixed $text
     */ 
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * @return mixed
     */ 
    public function getTime()
    {
        return $this->time;
    }

    /** 
     * @ORM\PrePersist()
     */ 
    public function setTimeOnPersist():void
    {
        $this->time = new \DateTime();
    }
    /** 
     * @param mixed $time
     */ 
    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * @return User
     */  
    public function getUser()
    {
        return $this->user;
    }

    /** 
     * @param mixed $user
     */ 
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection
     */ 
    public function getLikedBy()
    {
        return $this->likedBy;
    }
    public function like(User $user)
    {
        if($this->likedBy->contains($user)){
            return;
        }
        $this->likedBy->add($user);
    }
}
