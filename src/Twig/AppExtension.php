<?php
namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\Extension\GlobalsInterface;
use App\Entity\LikeNotification;

class AppExtension extends AbstractExtension implements GlobalsInterface
{
    public function getGlobals()
    {
        return [];
    }
    public function getTests()
    {
        return [
            new \Twig_SimpleTest('like', function($obj) {
                return $obj instanceof LikeNotification;
            })
        ];
    }
}