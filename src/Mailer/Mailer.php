<?php
namespace App\Mailer;

use App\Entity\User;

class Mailer
{
    /**
     * @var \Swift_Mailer
     */
    private $mailer;
    /**
     * @var \Twig_Environment
     */
    private $twig;
    /**
     * @var string
     */
    private $mailFrom;

    public function __construct(\Swift_Mailer $mailer,\Twig_Environment $twig,string $mailFrom)
    {
        $this->twig = $twig;
        $this->mailer = $mailer;
        $this->mailFrom = $mailFrom;
    }
    public function sendConformationMail(User $user)
    {
        $body = $this->twig->render('email/registration.html.twig',
            [
                'user' =>$user
            ]
        );

        $message = (new \Swift_Message())
               ->setSubject('welcome to my app')
               ->setFrom($this->mailFrom)
               ->setTo($user->getEmail())
               ->setBody($body,'text/html');
        $this->mailer->send($message);
    }
}