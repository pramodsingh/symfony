<?php
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use App\Entity\User as User;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

Class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('username',TextType::class)
        ->add('email',EmailType::class)
        ->add('plainPassword',RepeatedType::class,[
            'type' => PasswordType::class,
            'first_options'=>['label'=>'Password'],
            'second_options'=>['label'=>'Repated Password'],
        ])
        ->add('fullName',TextType::class)
        ->add('termAgreed',CheckboxType::class,[
            'mapped'=>false,
            'constraints'=> new IsTrue(),
            'label' => 'I agree to the terms of service'
        ])
        ->add('roles', ChoiceType::class, [
            'choices'  => [
                'ROLE_USER'=> 'ROLE_USER',
                'ROLE_ADMIN'=> 'ROLE_ADMIN',
            ],
            'multiple'  => true,
        ])
        ->add('Register',SubmitType::class);   
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class
        ]);
    }
}