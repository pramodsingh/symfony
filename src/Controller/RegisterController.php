<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\HttpFoundation\Request;
use App\Form\UserType;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use App\Event\UserRegisterEvent;
use App\Security\TokenGenerator;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;

class RegisterController extends Controller
{
    /**
     * @Route("/register", name="user_register")
     */
    public function register(UserPasswordEncoderInterface $passwordEncoder,Request $request,
                            EventDispatcherInterface $eventDisacher,TokenGenerator $tokenGenerator,FlashBagInterface $flashBag)
    { 
        $user = new User;
        //$user->setRoles([User::ROLE_USER]);

        $form = $this->createForm(UserType::class,$user);
        $form->handleRequest($request);
        $user->setEnabled(false);
        if($form->isSubmitted() && $form->isValid())
        {
            $password = $passwordEncoder->encodePassword(
                $user,
                $user->getPlainPassword()
            );
            $user->setPassword($password);
            $user->setConfirmationToken($tokenGenerator->getRandomSecureToken(30));
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();
            $userRegisterEvent = new UserRegisterEvent($user);
            $eventDisacher->dispatch(UserRegisterEvent::NAME,$userRegisterEvent);
            $flashBag->add('success','You have successfully registered.');
            return $this->redirect('micro-post/');
        }
        return $this->render('register/register.html.twig',[
            'form'=>$form->createView() 
        ]);
    }
    /**
     * @Route("/confirm/{token}", name="security_confirm")
     */

    public function confirm(string $token,UserRepository $userRepository,EntityManagerInterface $entityManager,\Twig_Environment $twig)
    {
        $user = $userRepository->findOneBy([
            'confirmationToken' => $token
        ]);
        if(null !== $user){
            $user->setEnabled(true);
            $user->setConfirmationToken('');
            $entityManager->flush();
        }
        return new Response($twig->render('security/confirmation.html.twig',[
                'user' => $user,
            ])
        );
    }
}