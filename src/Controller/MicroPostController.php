<?php
namespace App\Controller;

use App\Repository\MicroPostRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\MicroPost;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Form\FormFactoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\RouterInterface;
use App\Form\MicroPostType;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use App\Entity\User;
use App\Repository\UserRepository;

/**
 * @Route("/micro-post")
 */
Class MicroPostController
{
    /**
     * @var MicroPostRepository
     */
    private $microPostRepository;
    /**
     * @var \Twig_Environment
     */
    private $twig;
    /**
     * @var FormFactoryInterface
     */
    private $formFactory;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var RouterInterface
     */
    private $router;
    /**
     * @var FlashBagInterface
     */
    private $flashBag;
    
    public function __contruct(\Twig_Environment $twig, MicroPostRepository $microPostRepository,FormFactoryInterface $formFactory,EntityManagerInterface $entityManager,RouterInterface $router){
        $this->microPostRepository = $microPostRepository;
        $this->twig = $twig;
        $this->formFactory = $formFactory;
        $this->entityManager = $entityManager;
        $this->router = $router;
    }
    /**
     * @Route("/",name="micro_post_index")
     */
    public function index(
        TokenStorageInterface $tokenStorage, UserRepository $userRepository,\Twig_Environment $twig,MicroPostRepository $microPostRepository
    ) {
        $this->twig = $twig;
        $this->microPostRepository = $microPostRepository;
        $currentUser = $tokenStorage->getToken()
            ->getUser();
        $usersToFollow = [];
        
        if ($currentUser instanceof User) {
            $userFollowList = $currentUser->getFollowing();
            $userId=0;
            if(count($userFollowList)>0)
            {
                $userId=$currentUser->getId();
            }
            if(in_array('ROLE_ADMIN',$currentUser->getRoles())) {
                $posts = $this->microPostRepository->findBy(
                    [],
                    ['time' => 'DESC']
                );
            } else {
                $posts = $this->microPostRepository->findAllByUsers(
                    $userFollowList,$userId
                );
            }
            //$currentUser->getId()
            $usersToFollow = count($posts) === 0 ?
                $userRepository->findAllWithMoreThan5PostsExceptUser(
                    $currentUser
                ) : [];
        } else {
            $posts = $this->microPostRepository->findBy(
                [],
                ['time' => 'DESC']
            );
        }


        $html = $this->twig->render(
            'micro-post/index.html.twig',
            [
                'posts' => $posts,
                'usersToFollow' => $usersToFollow,
            ]
        );

        return new Response($html);
    }
    /*public function index(\Twig_Environment $twig,MicroPostRepository $microPostRepository,TokenStorageInterface $token){
        $this->twig = $twig;
        $this->microPostRepository = $microPostRepository;
        $currentUser = $token->getToken()->getUser();
       // echo "<pre>"; var_dump($currentUser->getFollowing());die('fff');
        if($currentUser instanceof User) {
            $posts = $this->microPostRepository->findAllByUsers($currentUser->getFollowing());
        } else {
            $posts = $this->microPostRepository->findBy([],['time'=>'DESC']);
        }
        $html = $this->twig->render('micro-post/index.html.twig',[
            'posts' => $posts
        ]);
        return new Response($html);
    }*/
    /**
     * @Route("/add",name="micro_post_add")
     * @Security("is_granted('ROLE_USER')")
     */
    public function add(Request $request,FormFactoryInterface $formFactory,RouterInterface $router,\Twig_Environment $twig,EntityManagerInterface $entityManager,TokenStorageInterface $tokenstorage,FlashBagInterface $flashBag){
        $user = $tokenstorage->getToken()->getUser();
        $this->formFactory = $formFactory;
        $this->router = $router;
        $this->twig = $twig;
        $this->entityManager = $entityManager;
        $this->flashBag = $flashBag;
        $microPost = new MicroPost();
        $microPost->setTime(new \DateTime());
        $microPost->setUser($user);
        $form = $this->formFactory->create(
            MicroPostType::class,$microPost
        );
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($microPost);
            $this->entityManager->flush();
            return new RedirectResponse($this->router->generate('micro_post_index'));
        }
        $this->flashBag->add('success','Micro post added successfully.');
        return new Response(
            $this->twig->render('micro-post/add.html.twig',
            ['form'=> $form->createView()]
            )
        );
    }
    /**
     * @Route("/edit/{id}",name="micro_post_edit", requirements={"id":"\d+"})
     * @Security("is_granted('edit',microPost)",message="Access denied")
     */
    public function edit(MicroPost $microPost,Request $request,FormFactoryInterface $formFactory,RouterInterface $router,\Twig_Environment $twig,EntityManagerInterface $entityManager){
        
        $this->formFactory = $formFactory;
        $this->router = $router;
        $this->twig = $twig;
        $this->entityManager = $entityManager;
        
        $form = $this->formFactory->create(
            MicroPostType::class,$microPost
        );
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($microPost);
            $this->entityManager->flush();
            return new RedirectResponse($this->router->generate('micro_post_index'));
        }
        return new Response(
            $this->twig->render('micro-post/add.html.twig',
            ['form'=> $form->createView()]
            )
        );
    }
    /**
     * @Route("/delete/{id}",name="micro_post_delete", requirements={"id":"\d+"})
     * @Security("is_granted('delete',microPost)",message="Access denied")
     */
    public function delete(MicroPost $microPost, EntityManagerInterface $entityManager,FlashBagInterface $flashBag,RouterInterface $router){
        
        $this->flashBag = $flashBag;
        $this->entityManager = $entityManager;
        $this->router = $router;
        $this->entityManager->remove($microPost); 
        $this->entityManager->flush(); 
        $this->flashBag->add('notice','Micro post was deleted.');
        return new RedirectResponse($this->router->generate('micro_post_index'));
    }
    /**
     * @Route("/user/{username}",name="micro_post_user")
     */
    public function userPosts(User $userWithPosts,\Twig_Environment $twig,MicroPostRepository $microPostRepository){
        $this->twig = $twig;
        $this->microPostRepository = $microPostRepository;
        $html = $this->twig->render('micro-post/user-posts.html.twig',[
            'posts' => $this->microPostRepository->findBy(['user'=>$userWithPosts],['time'=>'DESC']),
            'user' => $userWithPosts
            //'posts' => $userWithPosts->getPosts()
        ]);
        return new Response($html);
    }
    /**
     * @Route("/{id}",name="micro_post_post")
     */
    public function post(MicroPost $post,\Twig_Environment $twig,MicroPostRepository $microPostRepository){
        //$this->microPostRepository = $microPostRepository;
        //$post = $this->microPostRepository->find($id);
        $this->twig = $twig;
        return new Response(
            $this->twig->render('micro-post/post.html.twig',
                [
                    'post'=> $post
                ]
            )
        );
    }
}