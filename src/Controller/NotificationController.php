<?php
namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Repository\NotificationRepository;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use App\Entity\Notification;

/**
 * @Security("is_granted('ROLE_USER')")
 * @Route("/notification");
 */
class NotificationController extends Controller
{
    /**
     * @Route("/unread-count",name="notification_unread")
     */
    public function unreadCount(NotificationRepository $notificationRepo)
    {
        return new JsonResponse([
            'count' =>$notificationRepo->findUnseenByUser($this->getUser())
        ]);
    }
    /**
     * @Route("/all",name="notification_all")
     */
    public function notifications(NotificationRepository $notificationRepo)
    {
        return $this->render('notification/notifications.html.twig',[
            'notifications'=> $notificationRepo->findBy([
                'seen' => false,
                'user' =>$this->getUser()
            ])
        ]);
    }
    /**
     * @Route("/acknowledge/{id}",name="notification_acknowledge")
     */
    public function acknowledge(Notification $notification)
    {
        $notification->setSeen(true);
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('notification_all');
    }
    /**
     * @Route("/acknowledge-all",name="notification_acknowledge_all")
     */
    public function acknowledgeAll(NotificationRepository $notificationRepo)
    {
        $notificationRepo->markAllAsReadByUser($this->getUser());
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('notification_all');
    }
}