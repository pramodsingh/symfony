<?php
namespace App\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use App\Entity\User;

/**
 * @Security("is_granted('ROLE_USER')")
 * @Route("/following")
 */
class FollowingController extends Controller
{
    /**
     * @Route("/follow/{id}" , name="following_follow")
     */
    public function follow(User $userToFollow)
    {
        /** 
         * @var User $currentUser
         */
        $currentUser = $this->getUser();
        if($userToFollow->getId() !== $currentUser->getId()) {
            
            $currentUser->follow($userToFollow);
            $this->getDoctrine()->getManager()->flush();
        }
        
       // $this->getDoctrime()->getManager()->flush();
        return $this->redirectToRoute('micro_post_user',
            ['username'=>$userToFollow->getUsername()]
        );
    }
    /**
     * @Route("/unfollow/{id}" , name="following_unfollow")
     */
    public function unfollow(User $userToUnfollow)
    {
        /** 
         * @var User $currentUser
         */
        $currentUser = $this->getUser();
        if($userToUnfollow->getId() !== $currentUser) {
            $currentUser->getFollowing()->removeElement($userToUnfollow);
            $this->getDoctrine()->getManager()->flush();
        }
        return $this->redirectToRoute('micro_post_user',
            ['username'=>$userToUnfollow->getUsername()]
        );
    }
}