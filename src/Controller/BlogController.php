<?php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Routing\Annotation\Route;
/**
 * @Route("/blog")
 */
Class BlogController 
{
    /**
     * @var SessionInterface
     */
    private $session;
    /**
     * @var RouterInterface
     */
    private $router;
    /**
     * @var \Twig_Environment
     */
    private $twig;
    /**
     * @param \Twig_Environment $twig
     * @param SessionInterface $session
     * @param RouterInterface $router
     */
    public function __contruct(\Twig_Environment $twig, SessionInterface $session, RouterInterface $router){
        $this->session = $session;
        $this->twig = $twig;
    }
    /**
     * @Route("/index",name="blog_index")
     */
    public function index(\Twig_Environment $twig,SessionInterface $session){
        $this->twig = $twig;
        $this->session = $session;
        $html = $this->twig->render('blog/index.html.twig',
            [
                'posts'=>$this->session->get('posts')
            ]
        );
        return new Response($html);
    }
    /**
     * @Route("/add",name="blog_add")
     */
    public function add(SessionInterface $session,RouterInterface $router){
        $this->session = $session;
        $this->router = $router;
        $posts = $this->session->get('posts');
        $posts[uniqid()] = [
            'title' =>'A randam title '.rand(1,100),
            'text' =>'some testx nr '.rand(1,100),
        ];
        $this->session->set('posts',$posts);
        return new RedirectResponse($this->router->generate('blog_index'));
    }
    /**
     * @Route("/show/{id}",name="blog_show")
     */
    public function show($id,SessionInterface $session,\Twig_Environment $twig){
        $this->session = $session;
        $this->twig = $twig;
        $posts = $this->session->get('posts');
        if(!$posts || !isset($posts)) {
            throw new NotFoundHttpException('Post not found');
        }
        $posts[uniqid()] = [
            'title' =>'A randam title '.rand(1,100),
            'text' =>'some testx nr '.rand(1,100),
        ];
        $html = $this->twig->render('blog/post.html.twig',
            [
                'id' => $id,
                'post'=>$posts[$id],
            ]
        );
        return new Response($html);
    }
}