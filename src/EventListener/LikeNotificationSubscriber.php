<?php
namespace App\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Events;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\PersistentCollection;
use App\Entity\MicroPost;
use App\Entity\LikeNotification;

class LikeNotificationSubscriber implements EventSubscriber
{
    public function getSubscribedEvents()
    {
        return [
            Events::onFlush,
        ];
    }
    public function onFlush(OnFlushEventArgs $argc)
    {
        $em = $argc->getEntityManager();
        $uow = $em->getUnitOfWork();
        /**
         * @var PersistentCollection $collectionUpdate
         */
        foreach($uow->getScheduledCollectionUpdates() as $collectionUpdate)
        {
            if(!$collectionUpdate->getOwner() instanceof MicroPost)
            {
                continue;
            }
            if('likedBy' !== $collectionUpdate->getMapping()['fieldName']) {
                continue;
            }
            $insertDiff = $collectionUpdate->getInsertDiff();
            if(!count($insertDiff))
            {
                return;
            }
            /**
             * @var MicroPost $microPost
             */
            $microPost = $collectionUpdate->getOwner();
            $notification = new LikeNotification();
            $notification->setUser($microPost->getUser());
            $notification->setMicroPost($microPost);
            $notification->setLikedBy(reset($insertDiff));

            $em->persist($notification);
            $uow->computeChangeSet(
                $em->getClassMetadata(LikeNotification::class),
                $notification
            );
        }
    }
}