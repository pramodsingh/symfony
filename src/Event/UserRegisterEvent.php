<?php

namespace App\Event;

use Symfony\Component\EventDispatcher\Event;
use App\Entity\User;

class UserRegisterEvent extends Event 
{
    const NAME = 'user.register';
    /**
     * @var User
     */
    private $registerUser;

    public function __construct(User $registerUser)
    {
        $this->registerUser = $registerUser;
    }
    /**
     * @return User
     */
    public function getRegisteredUser(): User
    {
        return $this->registerUser;
    }
}
