<?php
namespace App\Event;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use App\Mailer\Mailer;

class UserSubscriber implements EventSubscriberInterface
{
    /**
     * @var Mailer
     */
    private $mailer;
    
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }
    public static function getSubscribedEvents()
    {
        return [
            UserRegisterEvent::NAME => 'OnUserRegister'
        ];
    }
    public function OnUserRegister(UserRegisterEvent $event)
    {
        $this->mailer->sendConformationMail($event->getRegisteredUser());
        /*$body = $this->twig->render('email/registration.html.twig',
            [
                'user' =>$event->getRegisteredUser()
            ]
        );

        $message = (new \Swift_Message())
               ->setSubject('welcome to my app')
               ->setFrom('admin@myapp.com')
               ->setTo($event->getRegisteredUser()->getEmail())
               ->setBody($body,'text/html');
        $this->mailer->send($message);
        */
    }
}